

from django import forms



class SubscriptionForm(forms.Form):
    name = forms.CharField()
    email = forms.CharField()